FROM ubuntu:18.04

ARG OPENSSL_RELEASE=NONE
ARG MINOR_VERSION=NONE

RUN echo $OPENSSL_RELEASE
RUN echo $MINOR_VERSION



# build tools installation
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install build-essential checkinstall zlib1g-dev -y



# transfering file into docker and unpacking
WORKDIR /usr/local/src/
ADD https://www.openssl.org/source/openssl-${OPENSSL_RELEASE}${MINOR_VERSION}.tar.gz .
RUN ls -l
RUN tar -xf openssl-${OPENSSL_RELEASE}${MINOR_VERSION}.tar.gz

WORKDIR /usr/local/src/openssl-${OPENSSL_RELEASE}${MINOR_VERSION}
RUN ./config --prefix=/usr/local/ssl --openssldir=/usr/local/ssl shared zlib
RUN make
RUN make test
RUN make install

WORKDIR /etc/ld.so.conf.d/
RUN echo "/usr/local/ssl/lib" > openssl-${OPENSSL_RELEASE}${MINOR_VERSION}.conf
RUN ldconfig -v

#RUN echo "$PATH:/usr/local/ssl/bin" > /etc/environment
#RUN source /etc/environment
#RUN echo $PATH
RUN ln -s /usr/local/ssl/bin/openssl /bin
RUN openssl version -a
